export const sampleFilter = (text, filterType='textField') => ({
    filterType: filterType,
    customFilterListOptions: { render: v => `${text}: ${v}` },
})

export const sampleDropdownFilter = (text, emptyText='Any') => ({
    customFilterListOptions: { render: v => `${text}: ${v ? v : emptyText}` },
    filterOptions: {
        renderValue: v => v ? v : emptyText
    }
})