const urlEncode = params => Object.entries(params).map(([k, v]) =>
    `${encodeURIComponent(k)}=${encodeURIComponent(v)}`).join('&');

const _submit = (url, config={}) => fetch(url, config)
		.then(r => r.status !== 200 ? _handlerError(r) : r.json())
		.then(r => r);

const _handlerError = xhr => {
	console.error(`ERROR ${xhr.status}: ${xhr.statusText}`);
	throw xhr;
};

export function getSubmit(baseUrl, data) {
	const url = data !== undefined ? `${baseUrl}?${urlEncode(data)}` : baseUrl;
	return _submit(url);
}

export function postSubmit(url, data={}) {
	return _submit(url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {'Content-Type': 'application/json'}
	});
}

export function deleteSubmit(url, data) {
	return _submit(url, {
		method: 'DELETE',
		body: JSON.stringify(data),
		headers: {'Content-Type': 'application/json'}
	});
}