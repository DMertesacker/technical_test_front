import * as React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Button, CircularProgress} from "@material-ui/core"

const useStyles = makeStyles(() => ({
	wrapper: {
		position: 'relative'
	},
    button: {
	    backgroundColor: '#1a73e8',
        color: '#f6f6f6'
    },
	buttonProgress: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
}));

export default function SubmitBtn({isLoading=false, onClick=null, children} = {}) {
	const classes = useStyles();

	return (
		<div className={classes.wrapper}>
			<Button type="submit" className={classes.button} onClick={onClick} variant="contained" disabled={isLoading}>
				{children}
			</Button>
			{isLoading && <CircularProgress className={classes.buttonProgress} size={25} />}
		</div>
	);
}