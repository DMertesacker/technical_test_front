import React from 'react'
import {Drawer} from "rsuite";

export default function DrawerComp({isOpen=false, controller, title='', backdrop=true, children}) {

    const handleClose = () => {
        if (isOpen) controller(false)
    }

    return (
        <Drawer backdrop={backdrop} show={isOpen} onHide={handleClose}>
            <Drawer.Header>
                <Drawer.Title>{title}</Drawer.Title>
            </Drawer.Header>
            <Drawer.Body className="p-12">
                {children}
            </Drawer.Body>
        </Drawer>
    )
}