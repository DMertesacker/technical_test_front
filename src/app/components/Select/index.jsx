import * as React from "react"
import Select from "react-select"
import SelectLabel from "./label";

const defaultHandleFilter = (label, value) => label.label.toLowerCase().includes(value.toLowerCase())

export default function SelectComp({
    // Comp props
    className='', value=null, options=[], placeholder='Select a option...', isMulti=false,
    // Functions
    onChange=null, filterOption=defaultHandleFilter,
    // Custom Props
    label, firstSelect=false, ...props
}) {

    if (options.length === 0)
        return <></>

    if (isMulti && value === null)
        value = []

    let optionSelected = isMulti
        ? options.filter(op => value.includes(op.value))
        : options.filter(op => op.value === value)

    if (!isMulti && optionSelected.length === 0)
        optionSelected = null

    if (!optionSelected  && firstSelect)
       optionSelected = options[0]

    const config = {
        className: `select ${className}`,
        options: options, placeholder: placeholder, isMulti: isMulti, closeMenuOnSelect: !isMulti,
        onChange: onChange, filterOption: filterOption,
        value: options.filter(op =>  isMulti ? value.includes(op.value) : value === op.value),
        ...props
    }

    return (<>
        <fieldset className={`select-wrapper`}>
            <SelectLabel label={label} selected={optionSelected} />
            <Select {...config} />
        </fieldset>
    </>)
}
