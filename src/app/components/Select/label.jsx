import * as React from "react"

export default function SelectLabel({label, selected}) {
    if (!label) return <></>

    return (<>
        <legend className={selected ? 'active' : null}>{label}</legend>
        <label className={selected ? 'active' : null}>{label}</label>
    </>)
}
