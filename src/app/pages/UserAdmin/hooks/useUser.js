import {useCallback, useContext} from "react"
import {UserAdminContext} from '../context'

export default function useUserAdmin() {
    const {state, setState} = useContext(UserAdminContext);
    const usersDataString = JSON.stringify(state.usersData)

	const updateState = useCallback(newState => {
	    setState(state => ({ ...state, ...newState}))
	}, [setState])

	const setLoading = useCallback(newState => {
	    updateState({isLoading: newState})
    }, [updateState])

    const setUsersData = useCallback(newState => {
        updateState({usersData: newState, isLoading: false})
    }, [updateState])

	const getDataRow = useCallback(index => {
		return state.usersData[index] || {}
	// eslint-disable-next-line
	}, [usersDataString])

	return {
	    isLoading: state.isLoading, usersData: state.usersData,

	    updateState, setLoading, setUsersData, getDataRow
    };
}