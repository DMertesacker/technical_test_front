import {useCallback, useContext} from "react"
import {UserAdminContext} from '../context'
import {UseStatistics, useUserAdmin} from "./index"
import {deleteSubmit, postSubmit} from "app/utils"
import {toast} from "react-hot-toast"

export default function useSingleUser() {
    const {state, setState} = useContext(UserAdminContext);
    const {getDataRow, usersData} = useUserAdmin()
    const {loadStatistics} = UseStatistics()
    const stateString = JSON.stringify(state)

	const updateState = useCallback(newState => {
	    setState(state => ({ ...state, ...newState}))
	}, [setState])

    const clearValues = useCallback(() => {
        updateState({dataIndex: null, userID: null, username: null, relUsers: []})
    }, [updateState])

	const setUserValues = useCallback(dataIndex => {
	    const {userID, name, relIDs} = getDataRow(dataIndex)
	    updateState({dataIndex: dataIndex, userID: userID, username: name, relUsers: relIDs})
    }, [updateState, getDataRow])

    const setSingleLoading = useCallback(newState => {
        updateState({singleLoading: newState})
    }, [updateState])

    const setUsername = useCallback(e => {
        updateState({username: e.target.value})
    }, [updateState])

    const setRelUsers = useCallback(options => {
        const newValues = options ? options.map(op => op.value) : []
        updateState({relUsers: newValues})
    }, [updateState])

    const onSubmit = useCallback(() => {
        setSingleLoading(true)
        return toast.promise(
            postSubmit('/api/user/update', {
                id_user: state.userID,
                name: state.username,
                rels: state.relUsers
            }).then(newRow => {
                const newData = [...usersData]
                newData[state.dataIndex] = newRow[0]
                updateState({usersData: newData, singleLoading: false})
                loadStatistics()
            }), {
                loading: 'Updating User...',
                success: <b>User Updated!</b>,
                error: () => {
                    setSingleLoading(false)
                    return <b>Error updating user. Please, try again.</b>
                },
            }
        )
    // eslint-disable-next-line
    }, [updateState, stateString, setSingleLoading])

    const deleteUser = useCallback((dataIndex, tID) => {
        const {userID} = getDataRow(dataIndex)
        toast.dismiss(tID)
        toast.promise(
            deleteSubmit('/api/user/delete', {
                id_user: userID
            }).then(() => {
                const newData = [...usersData]
                newData.splice(dataIndex, 1)
                updateState({usersData: newData})
                loadStatistics()
            }), {
                loading: 'Deleting User...',
                success: <b>User Deleted!</b>,
                error: <b>Error deleting user. Please, try again.</b>,
            }
        )
    // eslint-disable-next-line
    }, [getDataRow, updateState])

    const createUser = useCallback(() => {
        setSingleLoading(true)
        toast.promise(
            postSubmit('/api/user/new', {
                name: state.username,
                rels: state.relUsers
            }).then(res => {
                const newData = [...usersData]
                newData.push(res[0])
                updateState({usersData: newData, singleLoading: false})
                loadStatistics()
            }), {
                loading: 'Creating User...',
                success: <b>User Created!</b>,
                error: () => {
                    setSingleLoading(false)
                    return <b>Error Creating user. Please, try again.</b>
                },
            }
        )
    // eslint-disable-next-line
    }, [updateState, stateString])

	return {
	    singleLoading: state.singleLoading, userID: state.userID, username: state.username, relUsers: state.relUsers,

        updateState, setUserValues, setUsername, setRelUsers, onSubmit, deleteUser, clearValues, createUser
	}
}