import {useCallback, useContext} from "react"
import {UserAdminContext} from '../context'
import {getSubmit} from "../../../utils";

export default function UseStatistics() {
    const {state, setState} = useContext(UserAdminContext);

	const updateState = useCallback(newState => {
	    setState(state => ({ ...state, ...newState}))
	}, [setState])
	
	const setStatistics = useCallback(({userWithRels, userWithoutRels, top}) => {
	    updateState({userWithRels: userWithRels, userWithoutRels: userWithoutRels, top: top, loadingStatistics: false})
    }, [updateState])

    const loadStatistics = useCallback(() => {
        updateState({loadingStatistics: true})
        getSubmit('/api/user/statistics').then(r => setStatistics(r))
    }, [updateState, setStatistics])

	return {
	    loadingStatistics: state.loadingStatistics, userWithRels: state.userWithRels, userWithoutRels: state.userWithoutRels, top: state.top,

        updateState, setStatistics, loadStatistics
	};
}