import * as React from "react"
import MUIDataTable from "mui-datatables"
import {withStyles} from '@material-ui/core/styles'
import {LinearProgress} from "@material-ui/core";
import {nameColumn, optionsColumn, relationshipsColumn, userIdColumn} from "./columns";
import UserAdminTableCustomToolbar from "./CustomToolbar";

const StyledDataTable = withStyles({
  responsiveBase: {
        overflow: 'auto',
        padding: 0,
        maxHeight: 'calc(100vh - 147px)'
    },
})(MUIDataTable);


export default function UserAdminTable({data=[], isLoading=false}) {

    if (isLoading) return <LinearProgress/>

    const config = {
        title: 'List of Users',
        data: data,
        columns: [
            userIdColumn(), nameColumn(), relationshipsColumn(), optionsColumn()
        ],
        options: {
            filter: true,
            search: true,
            filterType: 'dropdown',
            fixedHeader: true,
            responsive: 'simple',
            sort: true,
            print: false,
            download: false,
            rowsPerPageOptions: [10, 25, 50, 100],
            selectableRows: 'none',
            tableBodyMaxHeight: 'auto',
            customToolbar: () => <UserAdminTableCustomToolbar />,
            customSearch: (searchQuery, currentRow) => {
                const coincidenceColumns  = currentRow.map(value => value ? value.toString().toLowerCase().includes(searchQuery.toLowerCase()) : false)
                return coincidenceColumns.includes(true)
            }
        }
    }

    return (<StyledDataTable {...config} />)
}
