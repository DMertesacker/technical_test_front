import * as React from "react"
import {useState} from "react"
import AddBoxIcon from '@material-ui/icons/AddBox';
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {Grid} from "@material-ui/core";
import {UsernameTextfield, UserSelect} from "../components";
import {DrawerComp, SubmitBtn} from "app/components";
import {useSingleUser} from "../hooks";

export default function UserAdminTableCustomToolbar() {
    const [isOpen, setOpen] = useState(false)
    const {singleLoading, clearValues, createUser} = useSingleUser()

    const handleClick = () => {
        setOpen(true)
        clearValues()
    }

    return (<>
        <span>
            <Tooltip title="New User">
                <IconButton onClick={handleClick} >
                    <AddBoxIcon />
                </IconButton>
            </Tooltip>
        </span>

        <DrawerComp isOpen={isOpen} controller={setOpen} title="New User">

            <Grid container spacing={1}>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                    <UsernameTextfield />
                </Grid>

                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                    <UserSelect />
                </Grid>

                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="flex flex-end">
                    <SubmitBtn className="w-100" isLoading={singleLoading} onClick={createUser} >
                        Update
                    </SubmitBtn>
                </Grid>
            </Grid>
        </DrawerComp>
    </>)
}
