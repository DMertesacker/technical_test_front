import * as React from 'react'
import {useUserAdmin} from "../../hooks"

export default function relationshipsColumn() {
    const label = 'Relationships'

    return {
        name: 'relNames', label: label,
        options: {
            filterType: 'textField',
            customFilterListOptions: {
                render: v => `${label}: ${v}`
            },
            filterOptions: {
                logic: (relationships, filters) =>
                    !relationships.join(',').toLowerCase().includes(filters)
            },
            customBodyRenderLite: dataIndex =>
                <RelationshipsCell dataIndex={dataIndex} />
        }
    }
}

const RelationshipsCell = ({dataIndex}) => {
    const {getDataRow} = useUserAdmin()
    const {relNames=[]} = getDataRow(dataIndex)

    return (<small>
        {relNames.join(', ')}
    </small>)
}