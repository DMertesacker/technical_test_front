import {sampleFilter} from "app/utils"

export default function userIdColumn() {
    const label = 'User ID'

    return {
        name: 'userID', label: label,
        options: {
            ...sampleFilter(label)
        }
    }
}