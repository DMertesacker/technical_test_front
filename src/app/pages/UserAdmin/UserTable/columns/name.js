import {sampleFilter} from "app/utils"

export default function nameColumn() {
    const label = 'Name'

    return {
        name: 'name', label: label,
        options: {
            ...sampleFilter(label)
        }
    }
}