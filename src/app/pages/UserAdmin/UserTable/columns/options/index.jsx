import * as React from 'react'
import UserAdminTableEditBtn from "./editBtn";
import UserAdminTableDeleteBtn from "./deleteBtn";

export default function optionsColumn() {
    return {
        name: '', label: 'Options',
        options: {
            filter: false, sort: false,
            customBodyRenderLite: dataIndex =>
                <IndexCell dataIndex={dataIndex} />
        }
    }
}

const IndexCell = ({dataIndex}) => {
    return (<div>
        <UserAdminTableEditBtn dataIndex={dataIndex} />
        <UserAdminTableDeleteBtn dataIndex={dataIndex} />
    </div>)
}