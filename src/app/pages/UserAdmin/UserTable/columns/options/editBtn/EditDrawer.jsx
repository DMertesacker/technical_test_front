import * as React from "react"
import {Grid} from "@material-ui/core"
import {SubmitBtn, DrawerComp} from "app/components"
import {UsernameTextfield, UserSelect} from "../../../../components"
import {useSingleUser} from "../../../../hooks"



export default function UserAdminTableEditDrawer({isOpen, controller}) {
    const {singleLoading, username, onSubmit, userID} = useSingleUser()

    const handleSubmit = () => {
        onSubmit().then(() => controller(false))
    }

    return (<DrawerComp isOpen={isOpen} controller={controller} title={`Update user ${username}`}>

        <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                <UsernameTextfield />
            </Grid>

            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                <UserSelect excluded={userID} />
            </Grid>

            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="flex flex-end">
                <SubmitBtn className="w-100" isLoading={singleLoading} onClick={handleSubmit} >
                    Update
                </SubmitBtn>
            </Grid>
        </Grid>
    </DrawerComp>)
}
