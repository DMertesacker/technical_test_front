import * as React from "react"
import {useState} from "react"
import EditIcon from '@material-ui/icons/Edit'
import UserAdminTableEditDrawer from "./EditDrawer";
import {useSingleUser} from "../../../../hooks";

export default function UserAdminTableEditBtn({dataIndex}) {
    const [isOpen, setOpen] = useState(false)
    const {setUserValues} = useSingleUser()

    const handleClick = () => {
        setUserValues(dataIndex)
        setOpen(true)
    }

    return (<>
        <EditIcon className="cursor-pointer" onClick={handleClick} />

        <UserAdminTableEditDrawer isOpen={isOpen} controller={setOpen} dataIndex={dataIndex}/>
    </>)
}
