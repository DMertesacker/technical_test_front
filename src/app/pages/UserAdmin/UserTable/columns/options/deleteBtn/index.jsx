import * as React from "react"
import DeleteIcon from '@material-ui/icons/Delete'
import {toast} from "react-hot-toast"
import {useSingleUser, useUserAdmin} from "../../../../hooks"


export default function UserAdminTableDeleteBtn({dataIndex}) {
    const {deleteUser} = useSingleUser()
    const {getDataRow} = useUserAdmin()
    const {name} = getDataRow(dataIndex)

    const handleClick = () => {
        toast((t) => (<>
            <span className="toast">
                <div className="content">Are you sure Delete user {name}?</div>

                <span className="actions">
                    <button className="confirm" onClick={() => deleteUser(dataIndex, t.id)}> Yes </button>
                    <button className="cancel" onClick={() => toast.dismiss(t.id)}> Cancel </button>
                </span>

            </span>
        </>))
    }

    return (<>
        <DeleteIcon className="cursor-pointer" onClick={handleClick}/>

    </>)
}
