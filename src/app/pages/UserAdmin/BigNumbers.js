import * as React from "react"
import {UseStatistics} from "./hooks"
import Paper from "@material-ui/core/Paper/Paper";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";

export default function UserAdminBigNumbers({...props}) {
    const {loadingStatistics, userWithRels, userWithoutRels} = UseStatistics()

    if (loadingStatistics) return <LinearProgress />

    return (<Paper style={{height: '100%'}}>
        <h2 className="p-24">
            Users With/out Relationships {userWithRels}/<small>{userWithoutRels}</small>
        </h2>
    </Paper>)
}
