import * as React from "react"
import {useEffect} from "react"
import UserAdminTable from "./UserTable"
import PageLayout from "../PageLayout"
import {getSubmit} from "app/utils"
import {UseStatistics, useUserAdmin} from "./hooks"
import Grid from "@material-ui/core/Grid"
import UserAdminTopUsers from "./TopUsers";
import UserAdminBigNumbers from "./BigNumbers";


export default function UserAdmin() {
    const {usersData, isLoading, setUsersData} = useUserAdmin()
    const {loadStatistics} = UseStatistics()

    useEffect(() => {
        getSubmit('/api/user/all-users')
            .then(r => setUsersData(r))

        loadStatistics()
    // eslint-disable-next-line
    }, [])

    return (
        <PageLayout title="User Admin">
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={9} lg={9} xl={9}>
                    <UserAdminBigNumbers />
                </Grid>

                <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
                    <UserAdminTopUsers />
                </Grid>

                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                    <UserAdminTable data={usersData} isLoading={isLoading} />
                </Grid>
            </Grid>
        </PageLayout>
    )

}
