import * as React from "react"
import {useEffect, useState} from "react"
import {SelectComp} from "app/components"
import {getSubmit} from "app/utils"
import {useSingleUser} from "../hooks";


export default function UserSelect({excluded=null, ...props}) {
    const [options, setOptions] = useState([])
    const {relUsers, setRelUsers} = useSingleUser()

    useEffect(() => {
        getSubmit('/api/user/user-options')
            .then(res => setOptions(res.filter(op => op.value !== excluded)))
    }, [excluded])

    const config = {
        options: options,
        isMulti: true, isClearable: true,
        value: relUsers, onChange: setRelUsers,
        ...props
    }

    return (<>
        <SelectComp {...config} />
    </>)
}
