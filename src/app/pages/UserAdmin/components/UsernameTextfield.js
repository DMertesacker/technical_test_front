import * as React from "react"
import {TextField} from "@material-ui/core";
import {useSingleUser} from "../hooks";

export default function UsernameTextfield({...props}) {
    const {username, setUsername} = useSingleUser()

    const config = {
        variant: 'outlined', label: 'Username', defaultValue: username,
        fullWidth: true, className: 'textfield',
        onChange: setUsername,
        ...props
    }

    return (<>
        <TextField {...config} />
    </>)
}
