import * as React from "react"
import {UseStatistics} from "./hooks"
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

export default function UserAdminTopUsers() {
    const {loadingStatistics, top} = UseStatistics()

    if (loadingStatistics) return <LinearProgress />

    return (<TableContainer component={Paper}>
        <Typography className="pl-16" variant="h6" component="div">
            <b>Top Users</b>
        </Typography>

        <Table size="small">
            <TableHead>
                <TableRow>
                    <TableCell>User</TableCell>
                    <TableCell align="right">Nº Relationships</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {top.map(({name, num}) => (
                    <TableRow key={name}>
                        <TableCell component="th" scope="row">
                            {name}
                        </TableCell>
                        <TableCell align="right">{num}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </TableContainer>)
}
