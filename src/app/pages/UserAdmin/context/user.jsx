import * as React from 'react'
import {useState} from 'react'

const Context = React.createContext({})

export function UserAdminContextProvider({children}) {
	const [state, setState] = useState({
	    isLoading: true, usersData: [],
		// Single user Form
		dataIndex: null, singleLoading: false, userID: null, username: null, relUsers: [],
		// Statistics
		loadingStatistics: false, userWithRels: null, userWithoutRels: null, top: []
	})

	return (
		<Context.Provider value={{state, setState}}>
			{children}
		</Context.Provider>
	)
}

export default Context