export default function PageLayout({title, subtitle='', children}) {
    return (<>
        <div className="m-14">
            <div className="page-title mb-14">

                <h3 className="page-title-element m-0">{title}</h3>
                {subtitle && <div className="page-title-element subtitle-element mx-8">{subtitle}</div>}
            </div>

            {children}
        </div>
    </>)
}
