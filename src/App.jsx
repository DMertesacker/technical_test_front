import React from "react"
import { Toaster } from 'react-hot-toast'
import UserAdmin from "./app/pages/UserAdmin"
import {UserAdminContextProvider} from "./app/pages/UserAdmin/context"

import 'rsuite/dist/styles/rsuite-default.css'
import './styles/_index.scss'

function App() {
  return (
      <UserAdminContextProvider>
          <div className="App">
              <UserAdmin />
          </div>
          <Toaster position="top-right" />
      </UserAdminContextProvider>
  )
}

export default App
